# Goals
- Categorise graphs: climbers, fallers, unknown
- Buy if yesterday's last price is much higher than the first of today
- Only needs to run when the exchange is open
- Chain stages (on command line?) (multi language pipeline)
- Evaluate price correlation: recommend diversifying
- If existing order is very close to recent minimum then reassess
- Large drops between close/open:: don't wait for price
- Dump all strategies with examples using plotter 
- Log scale 
- Use CSV format for intermediate data
- Plot recent minimum and new order 
- Report how close orders are to being executed
- Search all tokens for prospective trades with the right characteristics
- Generate test data from formulae (rather than drawing)
- Add markers as plot is created 
- Unicode markers: https://www.fileformat.info/info/unicode/char/2588/index.htm
- Sample input data instead and interpolate

# The environmental impact of trading
> One significant positive of Dogecoin is its relatively small energy use for
> transactions in comparison to other cryptocurrencies (0.12 kilowatt-hours per
> transaction (kWh). In comparison, Bitcoin uses 707.6 kWh and Ethereum uses
> 62.56 kWh.

https://en.wikipedia.org/wiki/Dogecoin

# Features
## Get prices from multiple sources
Get prices using web APIs and store in a common format (CSV).

```mermaid
graph TD
    etoro_api --- get_prices
    crypto_compare_api --- get_prices
    alpha_vantage_api --- get_prices
    marketstack_api --- get_prices

    get_prices --- read_json --- write_csv
```

## Review and update current BUYs
Read updated prices from files and check the current BUY order is still valid.
If not, cancel order and create a new one at a fixed percentage below the
current price.

```mermaid
graph TD
    read_prices_file --- get_recent_minimum_price --- calculate_new_price
    read_prices_file --- get_latest_price --- calculate_new_price

    calculate_new_price --- update_order
```

## Order summary
- Plot recent minima and new BUY price alongside recent prices
- Report how close orders are to being executed
- Search all tokens for prospective trades with the right characteristics

## Correlation checker
Check correlation of orders: recommend diversification if different assets
behave similiarly.

Naively this might involve comparing each set of prices with each other --
i.e., O(n<sup>2</sup>) -- but can this be done in linear time?

```mermaid
graph TD
    intermediate_format --- read_file
    
    read_file --- characterise_prices --- create_list_of_characteriations --- sort_list --- report_similar
```

# APIs
## Alpha Vantage
- https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=V&interval=1min&apikey=demo

## CryptoCompere
See a [Python example](https://gitlab.com/deanturpin/handt/-/blob/master/get_prices.py).

## Market stack / fixer.io
- https://marketstack.com/
- https://fixer.io/documentation

# Python example
```python
import requests

# Replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=5min&apikey=demo'
r = requests.get(url)
data = r.json()

print(data)
```

# C++20
- https://godbolt.org/z/oPevEras4

# Test data
## Steady decline
```
".                                                                            ",
" .                                                                           ",
"  ....                                                                       ",
"      ....          ....                                                     ",
"          ..........    ...                                                  ",
"                           .                                                 ",
"                            .            .                                   ",
"                             .            .                                  ",
"                              .   ..    .  .                                 ",
"                               . .  .  .         ..                          ",
"                                .     .     .   .  .                         ",
"                                     .       . .    .                        ",
"                                              .                              ",
"                                                     .                       ",
"                                                      .                      ",
"                                                       .    ..               ",
"                                                        .  .             .   ",
"                                                         .              . .  ",
"                                                          .   .            . ",
"                                                               .........    .",
```
https://godbolt.org/z/vMha4Yshq

## Sudden drop
```
".                                                                            ",
" .                ...                                                        ",
"  ................   ......                                                  ",
"                                                                             ",
"                                                                             ",
"                           .                                                 ",
"                                                                             ",
"                                                                             ",
"                                                                             ",
"                            .                                                ",
"                                                                             ",
"                                                                             ",
"                                                                             ",
"                             .                                               ",
"                                                                             ",
"                                                                             ",
"                                                                         .   ",
"                              .                                         . .  ",
"                               ...    ...       ....      ..               . ",
"                                  ....   .......    ......  ............    .",
```

# Python
https://godbolt.org/z/xcvhYTa1E

# Sorting index/value pair in c++
https://godbolt.org/z/5zesT1ens

# cpp ideas
```c++
  // struct stats {
  //   std::pair<size_t, double> min_;
  //   std::pair<size_t, double> max_;
  // };
```

# References
- [List of electronic trading protocols](https://gitlab.com/deanturpin/tradar)
- [QuickFIX](https://gitlab.com/deanturpin/tradar)
- [Protocol Buffers](https://gitlab.com/deanturpin/tradar)
- [Financial Information eXchange](https://gitlab.com/deanturpin/tradar)
