#include <algorithm>
#include <string>
#include <vector>
#include <fstream>
#include <iterator>
#include <fmt/core.h>

#include "plot.h"

int main(int argc, char* argv[]) {

  // Get prices file from command line if provided
  const std::string file{argc == 2 ? argv[1] : "prices/example.csv"};
  std::ifstream in{file};
  const std::vector<double> prices(std::istream_iterator<double>{in}, {});

  // Read prices and plot
  const auto &[min, max] = std::ranges::minmax_element(prices);

  fmt::print("Read {} value from {}\n", prices.size(), file);
  fmt::print("{}", plot(prices, 40));
}
