#include <vector>
#include <algorithm>
#include <string>
#include <type_traits>
#include <cassert>
#include <cmath>

// To run, copy this whole file into Godbolt, gcc trunk with --std=c++2b and -lfmt library addded

template <typename A, typename B>
std::string price_slice(const std::vector<A>& prices, const B& threshold) {

  assert(not prices.empty());
  static_assert(std::is_arithmetic_v<A> and std::is_arithmetic_v<B>, "Prices type must be arithmetic");

  const auto _threshold = static_cast<A>(threshold);

  std::string out{std::to_string(_threshold) + "\t"};
  std::ranges::for_each(prices, [&](const auto& x) {
    out += (x < _threshold ? ' ' : '|');
  });

  return out;
}

template <typename A>
std::string dump_plot(const std::vector<A>& prices, const size_t num_rows) {

  // Get price limits
  const auto& [_min, _max] = std::ranges::minmax_element(prices);
  const auto min = *_min;
  const auto max = *_max;
  const auto range = max - min;
  const auto increment = range / num_rows;

  // Create rows by decrementing threshold each pass
  std::string out;
  for (size_t i{0}; i < num_rows; ++i) {
    const auto threshold = A{max - (i * increment)};
    out += price_slice(prices, threshold) + "\n";
  }

  return out;
}


/*
5 points into 4 points

|   |   |   |   |   .
|    |    |    |    .

*/

template <typename A>
auto scale_x(const std::vector<A>& series, const size_t bins) {

  // Series requires scaling
  if (bins < series.size()) {
    const double increment = static_cast<double>(series.size()) / static_cast<double>(bins);
    auto scaled_series = std::vector<A>{};
    scaled_series.reserve(bins);

    for (size_t i{0}; i < bins; ++i) {

      // Calculate inbetween value
      const size_t left_index = static_cast<size_t>(std::floor(increment * i));
      const size_t right_index = static_cast<size_t>(std::ceil(increment * i));

      const auto left = series.at(left_index);
      const auto right = series.at(right_index);

      const auto interpolated_value = std::lerp(left, right, 0.5);

      scaled_series.push_back(interpolated_value);
    }

    return scaled_series;
  }

  // No scaling necessary, just return original data
  return series;
}

template <typename A>
std::string plot(const std::vector<A>& prices, const size_t num_rows) {

  // Rescale if necessary
  const auto max_width = size_t{80};
  return dump_plot(scale_x(prices, max_width), num_rows);
}

// Unit tests -- to be compiled in Godbolt but not production
#ifndef NDEBUG
#include <fmt/core.h>
#include <fmt/ranges.h>

void test_scaling() {
  {
    const auto input = std::vector{1, 2, 3, 4, 5};
    fmt::print("{}\n", input);
    assert(scale_x(input, 20).size() == input.size());
  }
  {
    const auto input = std::vector{1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 1, 2, 3, 4, 5,};
    fmt::print("{}\n", input);
    assert(scale_x(input, 20).size() == 20);
  }
}

void test_rows() {
  {
    const auto input = std::vector<double>{10, 12, 14, 15, 16, 15, 14, 13, 12, 11};
    const auto actual = price_slice(input, 16.0);
    fmt::print("1\t'{}'\n", actual);
    assert(actual == "16.000000	     |    ");
  }

  {
    const auto input = std::vector<double>{1, 2, 4, 5, 6, 5, 4, 3, 2, 1};
    const auto actual = price_slice(input, 4.0);
    fmt::print("2\t'{}'\n", actual);
    assert(actual == "4.000000	   |||||  ");
  }

  {
    const auto input = std::vector<double>{1, -1, 1, -1, 1, -1, 1, -1, 1, -1};
    const auto actual = price_slice(input, 0.0);
    fmt::print("3\t'{}'\n", actual);
    assert(actual == "0.000000	 | | | | |");
  }

  {
    const auto input = std::vector<double>{1, 2, 4, 5, 6, 5, 4, 3, 2, 1};
    const auto actual = price_slice(input, -6.0);
    fmt::print("4\t'{}'\n", actual);
    assert(actual == "-6.000000	||||||||||");
  }
  {
    const auto input = std::vector<double>{1, 2, 4, 5, 6, 5, 4, 3, 2, 1};
    const auto actual = price_slice(input, 2);
    fmt::print("5\t'{}'\n", actual);
    assert(actual == "2.000000	 |||||||| ");
  }
}

void test_plotter() {

  // Wide data that require interpolation
  {
    const auto input = std::vector<double>{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, };

    const auto actual = plot(input, 10);
    fmt::print("{}\tsize {}\n\n", actual, actual.size());
  }

  {
    const auto input = std::vector<double>{2922, 17816, 27341, 28778, 19053, 24617, 23777, 14453, 18429, 13326, 5545, 15366, 30498, 11821, 1031};
    fmt::print("{}", plot(input, 10));
  }

  {
    const auto input = std::vector<double>{50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60};
    fmt::print("{}", plot(input, 10));
  }
}

int main() {

  fmt::print("SCALING\n");
  test_scaling();

  fmt::print("PLOT\n");
  test_plotter();

  fmt::print("SLICE\n");
  test_rows();

}
#endif
