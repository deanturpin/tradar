import json
import os

# Convert JSON price data into CSV

prices_dir = 'prices'

for file in os.listdir(prices_dir):
    if file.endswith(".json"):

        # Construct pair of in/out filenames
        original_file = os.path.join(prices_dir, file)
        converted_file = os.path.join(prices_dir, file.split('.')[0] + '.csv')
        print('Converting', original_file, 'to', converted_file)

        try:
            # Load JSON data            
            f = open(original_file, 'r')
            prices = json.load(f)

            # Extract price data and write to CSV            
            for point in prices['data']:
                with open(converted_file, 'w') as csv_data:

                    # Write header
                    # csv_data.write('date,open,high,low,close,volume\n')

                    # Write data
                    for point in prices['data']:
                        csv_data.write(f"{point['open']}\n")
                        # csv_data.write(f"{point['date']},{point['open']},{point['high']},{point['low']},{point['close']},{point['volume']},\n")

        except Exception as e:
            print('generated exception: ' + str(e))

    else:
        print(f'Skipping file \"{file}\"')
