readonly symbols_file=symbols.txt
if [[ ! -e $symbols_file ]]; then
    echo $symbols_file does not exist, cya
    exit
fi

# Get API key from environmet
if [[ -z "${MARKETSTACK_API_KEY}" ]]; then
    echo "MARKETSTACK_API_KEY not defined"
    exit 1
fi

# Create prices dir if it doesn't exist
readonly prices_dir=prices
mkdir -p $prices_dir

readonly period=intraday
readonly interval=15min
cat $symbols_file | while read symbol; do
    url="http://api.marketstack.com/v1/$period?access_key=$MARKETSTACK_API_KEY&symbols=$symbol&interval=$interval"
	echo Get $period for $symbol
    echo $url
	curl --silent "$url" -o $prices_dir/$symbol.json
done

