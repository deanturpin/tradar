import statistics
import sys
import csv

if not sys.argv[1]:
    print("no file on command line")
    exit

csv_file_name = sys.argv[1]
print(csv_file_name)

# Extract open prices
prices = []
dates = []
with open(csv_file_name) as csv_file:

    # Skip the header
    csv_reader = csv.reader(csv_file)
    next(csv_reader)
    for row in csv_reader:
        prices.append(float(row[1]))

# print(prices)
latest = prices[2]
minimum = min(prices)
maximum = max(prices)
mean = statistics.mean(prices)
drift = max(abs(latest - minimum), abs(latest - maximum))
drift_percentage = 100.0 * drift / mean
standard_deviation = statistics.stdev(prices)

# Unit tests
# https://godbolt.org/z/7qP9dK8Wq

# Report stats
print("drift\t", drift)
print("drift%\t", drift_percentage)
print("last\t", latest)
print("BUY@\t", latest * .9)
print("min\t", minimum)
print("max\t", maximum)
# print("med L\t", statistics.median_low(prices))
print("med\t", statistics.median(prices))
# print("med H\t", statistics.median_high(prices))
print("mod\t", statistics.mode(prices))
# print("geo\t", statistics.geometric_mean(prices))
# print("har\t", statistics.harmonic_mean(prices))
print("mean\t", mean)
# print("fmean\t", statistics.fmean(prices))
print("stddev\t", standard_deviation)
print("std%\t", 100.0 * standard_deviation / mean)
# print("pstdev\t", statistics.pstdev(prices))
print("var\t", statistics.variance(prices))
# print("pvar\t", statistics.pvariance(prices))
print("quant\t", statistics.quantiles(prices))
print('--------')